const api = 'https://pokeapi.co/api/v2/pokemon';
let count = null;

let $pokemon = $('.pokemon');
let $button = $('.button');
let $sprite = $('.image img');
let $name = $('.name');

class Pokemon {
    getCount() {
        fetch(`${api}`, {mode: 'cors'})
            .then(response => response.json())
            .then(response => this.getPokemon(response.count))
            .catch(console.warn);
    }
    
    getPokemon(data) {
        count = data;
        if (count === null) {
            this.getCount();
        } else {
            fetch(`${api}/${Math.floor(Math.random() * (count - 1)) + 1}`, {mode: 'cors'})
                .then(response => response.json())
                .then(response => this.render(response))
                .catch(console.warn);
        }
    }
    
    render(pokemon) {
        $sprite.attr('src', pokemon.sprites.front_default);
        $name.html(pokemon.name);
    }
}

const pokemon = new Pokemon();

$button.click(() => {
  pokemon.getPokemon(count);
  if ($pokemon.hasClass('hidden')) {
    $pokemon.removeClass('hidden');
  }
});